<?php

namespace Drupal\history_memory_calculator\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Block to display last visited pages you visited on the website.
 *
 * @Block(
 *   id = "history_memory_calculator_block",
 *   admin_label = @Translation("History Memory Calculator Block")
 * )
 */
class CalculatorBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $this->configuration['layout'] = 1;
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $uid = \Drupal::currentUser()->id();
    $content = [];
    // Calculator display layout.
    $layout = $this->configuration['layout'] ?: 1;

    $content['layout'] = $layout;

    $build = [];
    $build['calculator'] = [
      '#theme' => 'history_memory_calculator_block',
      '#content' => $content,
    ];

    $build['#attached']['library'][] = 'history_memory_calculator/history_memory_calculator.frontend';
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Calculator Layout'),
      '#description' => $this->t('Select the available calculator layouts.'),
      '#default_value' => $config['layout'] ?? '1',
      '#options' => [
        '1' => $this->t('Layout 1'),
      ],
      '#attributes' => ['class' => ['calculator-layout']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['layout'] = $form_state->getValue('layout');
  }

  /**
   * {@inheritdoc}
   *
   * Disable block cache to keep it the Calculator Block update.
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
