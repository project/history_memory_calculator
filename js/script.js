function calculateExpression(expression) {
  const precedence = {
    'x': 2, // Multiplication
    '/': 2, // Division
    '%': 2, // Modulus
    '+': 1, // Addition
    '-': 1, // Subtraction
  };
  const stack = [];
  const operators = "+-x/%";
  const tokenize = (expression) => expression.match(/(?:\d+\.\d*|\d*\.?\d+|[-+x/%()])/g) || [];
  
  const shuntingYard = (tokens) => {
    const output = [];
    for (const token of tokens) {
      if (!isNaN(token)) {
        output.push(parseFloat(token));
      } else if (operators.includes(token)) {
        while (
          stack.length > 0 &&
          operators.includes(stack[stack.length - 1]) &&
          precedence[token] <= precedence[stack[stack.length - 1]]
        ) {
          output.push(stack.pop());
        }
        stack.push(token);
      } else if (token === "(") {
        stack.push(token);
      } else if (token === ")") {
        while (stack[stack.length - 1] !== "(") {
          output.push(stack.pop());
        }
        stack.pop(); // Remove the opening parenthesis
      }
    }
    while (stack.length > 0) {
      output.push(stack.pop());
    }
    return output;
  };

  const evaluateRPN = (tokens) => {
    const stack = [];
    for (const token of tokens) {
      if (!isNaN(token)) {
        stack.push(token);
      } else if (operators.includes(token)) {
        const b = stack.pop();
        const a = stack.pop();
        switch (token) {
          case "+":
            stack.push(a + b);
            break;
          case "-":
            stack.push(a - b);
            break;
          case "x":
            stack.push(a * b);
            break;
          case "/":
            stack.push(a / b);
            break;
          case "%":
            stack.push(a % b);
            break;
        }
      }
    }
    return stack[0];
  };

  const tokens = tokenize(expression);
  const postfix = shuntingYard(tokens);
  return evaluateRPN(postfix);
}

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.calculator_behavior = {
    attach: function (context, settings) {
      if ($(".block-history-memory-calculator-block").length > 0) {
        $(".block-history-memory-calculator-block").each(function (i, obj) {
          var id = $(obj).attr("id");
          if ($("#" + id).length > 0) {
            const display1El = document.querySelector("#" + id + " .display-1");
            const display2El = document.querySelector("#" + id + " .display-2");
            const numbersEl = document.querySelectorAll("#" + id + " .number");
            const operationEl = document.querySelectorAll(
              "#" + id + " .operation"
            );
            const equalEl = document.querySelector("#" + id + " .equal");
            const clearLastEl = document.querySelector(
              "#" + id + " .all-clear"
            );
            const clearAllEl = document.querySelector(
              "#" + id + " .last-entity-clear"
            );
            const memoryStoreEl = document.querySelector(
              "#" + id + " .memory-store"
            );
            const memoryHistoryEl = document.querySelector(
              "#" + id + " .memory-history"
            );
            const clearMemoryEl = document.querySelector(
              "#" + id + " .clear-memory"
            );
            const deleteButton = document.querySelector(
              "#" + id + " .delete"
              );

            let dis1Num = "";
            let dis2Num = "";
            let result = null;
            let lastOperation = "";
            let haveDot = false;
            let memoryHistory = [];
            let currentPage = 0;
            const entriesPerPage = 2;
            let isDisplay2Empty = true;
            let calculationComplete = true; // Start with true
            let memoryValue = null;


            numbersEl.forEach((number) => {
              number.addEventListener("click", (e) => {
                if (e.target.innerText === "." && !haveDot) {
                  haveDot = true;
                } else if (e.target.innerText === "." && haveDot) {
                  return;
                }

                if (isDisplay2Empty || lastOperation === "=") {
                  dis2Num = e.target.innerText;
                  isDisplay2Empty = false;
                  calculationComplete = false; // User entered a new digit
                } else {
                  dis2Num += e.target.innerText;
                }

                display2El.innerText = dis2Num;
              });
            });

            operationEl.forEach((operation) => {
              operation.addEventListener("click", (e) => {
                if (!dis2Num) return;
                haveDot = false;
                const operationName = e.target.innerText;
                if (dis1Num && dis2Num && lastOperation) {
                  mathOperation();
                } else {
                  result = parseFloat(dis2Num);
                }
                clearVar(operationName);
                lastOperation = operationName;
              });
            });

            function clearVar(name = "") {
              dis1Num += dis2Num + " " + name + " ";
              display1El.innerText = dis1Num;
              display2El.innerText = "";
              dis2Num = "";
            }

            function mathOperation() {
              if (lastOperation === "x") {
                result = parseFloat(result) * parseFloat(dis2Num);
              } else if (lastOperation === "+") {
                result = parseFloat(result) + parseFloat(dis2Num);
              } else if (lastOperation === "-") {
                result = parseFloat(result) - parseFloat(dis2Num);
              } else if (lastOperation === "/") {
                result = parseFloat(result) / parseFloat(dis2Num);
              } else if (lastOperation === "%") {
                result = parseFloat(result) % parseFloat(dis2Num);
              }
            }

            // Existing code...
            deleteButton.addEventListener("click", () => {
              if (dis2Num) {
                // If display 2 is not empty, remove the last character from it.
                dis2Num = dis2Num.slice(0, -1);
                display2El.innerText = dis2Num;
              } else if (dis1Num) {
                // If display 2 is empty but display 1 is not, remove the last character from display 1.
                dis1Num = dis1Num.slice(0, -1);
                display1El.innerText = dis1Num;
              }
            });

           equalEl.addEventListener("click", () => {
            if (!result || calculationComplete) return;
              haveDot = false;

         // Replace the mathOperation call with calculateExpression
             result = calculateExpression(dis1Num + dis2Num);
             calculationComplete = true; // Calculation is complete

            const calculation = `${dis1Num} ${dis2Num} = ${result}`;
            clearVar("=");
            
            display1El.innerText = calculation;
            display2El.innerText = result;
             dis2Num = result;
             dis1Num = "";
            document.querySelector("#calc-typed").innerText = "";
            isDisplay2Empty = true;
            memory();          
          });

            clearLastEl.addEventListener("click", () => {
              dis1Num = "";
              dis2Num = "";
              display1El.innerText = "";
              display2El.innerText = "";
              result = "";
              calculationComplete = true; // Clear also resets calculation status
            });

            clearAllEl.addEventListener("click", () => {
              dis2Num = "";
              memoryValue = null;
              memoryHistory = [];
              display2El.innerText = "";
              updateMemoryHistory();
              localStorage.removeItem("calculator_memory_history");
            });

            if (localStorage.getItem("calculator_memory_history")) {
              memoryHistory = JSON.parse(
                localStorage.getItem("calculator_memory_history")
              );
              updateMemoryHistory();
            }
             function memory(){
            // memoryStoreEl.addEventListener("click", () => {
              if (!dis2Num && dis1Num) return;
              if (!result) return;
              const calculation = `${dis1Num.trim()} ${display1El.innerText} `;
              const currentDate = getCurrentDate();
              const currentTime = getCurrentTime();

              memoryHistory.push({
                calculation,
                date: currentDate,
                time: currentTime,
              });
              updateMemoryHistory();

              const newPage = Math.floor(memoryHistory.length / entriesPerPage);
              currentPage = newPage;

              localStorage.setItem(
                "calculator_memory_history",
                JSON.stringify(memoryHistory)
              );
            //});
              }
  
            function getCurrentDate() {
              const now = new Date();
              const year = now.getFullYear();
              const month = (now.getMonth() + 1).toString().padStart(2, "0");
              const day = now.getDate().toString().padStart(2, "0");
              return `${year}/${month}/${day}`;
            }

            function getCurrentTime() {
              const now = new Date();
              const hours = now.getHours().toString().padStart(2, "0");
              const minutes = now.getMinutes().toString().padStart(2, "0");
              return `${hours}:${minutes}`;
            }

            function updateMemoryHistory() {
              const memoryHistoryTable = document.querySelector(
                ".memory-history-table tbody"
              );
              memoryHistoryTable.innerHTML = "";
              const startIndex = currentPage * entriesPerPage;
              const endIndex = startIndex + entriesPerPage;

              const displayedEntries = memoryHistory.slice(
                startIndex,
                endIndex
              );

              displayedEntries.forEach((entry) => {
                const newRow = document.createElement("tr");
                newRow.innerHTML = `
                  <td>${entry.calculation}</td>
                  <td>${entry.date}</td>
                  <td>${entry.time}</td>
                `;

                memoryHistoryTable.appendChild(newRow);
              });

              const previousButton = document.querySelector(".previous-button");
              const nextButton = document.querySelector(".next-button");
            }

            const previousButton = document.querySelector(".previous-button");

            previousButton.addEventListener("click", () => {
              if (currentPage > 0) {
                currentPage--;
                updateMemoryHistory();
              }
            });

            const nextButton = document.querySelector(".next-button");

            nextButton.addEventListener("click", () => {
              const startIndex = currentPage * entriesPerPage;
              const endIndex = startIndex + entriesPerPage;
              if (endIndex < memoryHistory.length) {
                currentPage++;
                updateMemoryHistory();
              }
            });
          }
        });
      }
    },
  };
})(jQuery, Drupal, drupalSettings);
