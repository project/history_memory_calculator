# History Memory Calculator

- "History Memory Calculator" is designed to provide a calculator tool with memory 
  and history functionality as a block that can be added to Drupal websites.

## Table of contents

- Introduction
- Description
- Requirements
- Installation
- Use Cases
- Maintainers

## Introduction

- The History Memory Calculator module is a versatile and user-friendly tool that 
  allows Drupal website users to perform calculations while keeping track of their 
  calculation history and storing values in memory. This module adds a calculator 
  block to your Drupal site, enabling you to offer a powerful and convenient 
  calculator tool to your site visitors.

## Description

- Calculator Block: Easily add the calculator as a block to any region on your 
  Drupal site.
- Memory Storage: Store and recall previous calculations with the memory 
  functionality.
- Calculation History: Keep a record of past calculations with date and time 
  stamps.
- Navigation: Navigate through calculation history with pagination for a seamless 
  user experience.
- Clear Functionality: Clear the calculator's display, memory, or entire history 
  when needed.
- Math Operations: Perform basic math operations like addition, subtraction, 
  multiplication, division, and percentage calculations.
- Decimal Support: Perform calculations with decimal numbers, including floating- 
  point operations.
- Responsive Design: The calculator is designed to work seamlessly on various 
  screen sizes and devices.
   
## Requirements

- This module requires no modules outside of Drupal core.

## Installation

- Install this module as you would normally install
  a contributed Drupal module.
  [Visit](https://www.drupal.org/node/1897420) for further information.

## Use Cases

- Educational websites can use this module to provide students with a practical online calculator.
- E-commerce sites can offer customers a useful tool for price calculations.
- Financial websites can assist users with investment and savings calculations.
- Enhance your Drupal website's functionality by adding the History Memory 
  Calculator module, providing a feature-rich calculator tool that will benefit 
  your users.

## Maintainers
- Soumya Soni - [soumya soni](https://www.drupal.org/u/soumya_soni)



 